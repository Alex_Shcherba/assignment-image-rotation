#include "input/for_bmp.h"
#include "files/files.h"
#include "transformation/rotate_image.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc < 3) {
        fprintf(stderr, "Не хватает аргументов");
        return -1;
    }

    char* input_filename = argv[1];
    char* output_filename = argv[2];

    FILE* file_input = NULL;
    FILE* file_output = NULL;
    bool file_input_opened = open_file(&file_input, input_filename, "rb");
    bool file_output_opened = open_file(&file_output, output_filename, "wb");

    if (file_input_opened) {
    	if (!file_output_opened) {
    	    close_file(&file_input);
    	    fprintf(stderr, "проблемы с выходным файлом \n");
    	    return -1;
    	} else {
    		fprintf(stdout, "файлы открыты \n");
    	}
    } else {
        if (!file_output_opened) {
            fprintf(stderr, "файлы не существуют или нет доступа \n");
        }
        else {
            close_file(&file_output);
            fprintf(stderr, "входной файл не существует или нет доступа \n");
        }
        return -1;
    }

    struct image my_image = {0,0,NULL};
    if (from_bmp(file_input, &my_image)!= 0) {
        fprintf(stderr, "Указан файл неверного формата \n");
        return -1;
    }
    fprintf(stdout, "Файл прочитан \n");

    struct image rotated_image = rotate(my_image);
    image_delete(my_image);
    
    if (to_bmp(file_output, &rotated_image) != 0) {
        fprintf(stderr, "Ошибка конвертации в .bmp \n");
        return -1;
    }
    image_delete(rotated_image);
    
    close_file(&file_input);
    close_file(&file_output);
    fprintf(stdout, "всё отлично! \n");

    return 0;
}

