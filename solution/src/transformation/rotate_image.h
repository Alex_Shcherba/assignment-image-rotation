#ifndef ROTATE_IMAGE
#define ROTATE_IMAGE

#include "../image/image.h"
#include <stdlib.h>

struct image rotate(struct image const source);

#endif