#include "rotate_image.h"

struct image rotate(struct image const source){
    const uint64_t width = source.width;
	const uint64_t height = source.height;
	struct pixel* data = source.pixels;
	struct image res;
	res.width = height;
	res.height = width;
	res.pixels =  malloc(sizeof(struct pixel)*width*height);
	
    struct pixel *copy_pixels = res.pixels;

	for(size_t i = 0; i < height; i++){
		for(size_t j = 0; j < width; j++){
			*(copy_pixels + j * height + (height - i - 1)) = *(data + i * width+j);
		}
	}
	return res;	
}
