#include "image.h"
#include "stdlib.h"

uint32_t image_size_row(struct image const* img){
    return sizeof (struct pixel) * img->width;
}

struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y){
    return img.pixels[x + y * img.width];
}

struct image* image_create(struct image* img, uint64_t width, uint64_t height){
	img->height = height;
    img->width = width;
    img->pixels = malloc (width * height * sizeof (struct pixel));
	return img;
}

void image_delete(struct image img){
	free(img.pixels);
}
