#ifndef IMAGE
#define IMAGE

#include <stdint.h>
#include <stdio.h>

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};

struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

uint32_t image_size_row(struct image const* img);
struct pixel get_pixel(const struct image img, const uint64_t x, const uint64_t y);

struct image* image_create(struct image* img, size_t width, size_t height);

void image_delete(struct image img);

#endif
