#ifndef FILES_H
#define FILES_H
#include <stdbool.h>
#include <stdio.h>

bool open_file(FILE** myFile, const char* myFilename, const char* openMode);
bool close_file(FILE** myFile);

#endif
