#include "for_bmp.h"

#include <stdlib.h>
static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

size_t bmp_padding(uint64_t width){
    return 4 - (width * 3) % 4;
}

static void fill_padding(struct image *img, FILE *out){
    uint8_t newPadding = bmp_padding(img->width);
    uint8_t paddings[4];
    for (size_t i = 0; i < newPadding; i++) {
        paddings[i] = 0;
    }
    fwrite(paddings, newPadding, 1, out);
}

uint32_t image_bmp_size(struct image *img){
    return (sizeof(struct pixel) * img->width * img->height + bmp_padding(img->width)) * img->height;
}

struct bmp_header create_header(struct image *img) {
    struct bmp_header header;
    header.bfType = bfType;
    header.bfileSize = sizeof(struct bmp_header) + image_bmp_size(img);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = biSize;
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biPlanes = 1;
    header.biBitCount = biBitCount;
    header.biCompression = 0;
    header.biSizeImage = image_bmp_size(img);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_stat from_bmp(FILE *in, struct image* img){
    struct bmp_header header = {0};
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        fprintf(stderr, "Error from reading header\n");
        return RINVALID_HEADER;
    }
    if (header.biBitCount != 24 || !in){
        fprintf(stderr, "Not supported bit count \n");
        return RINVALID_COUNT;
    }
    if (header.bfType != bfType){
        return RINVALID_SIGNATURE;
    }

    img = image_create(img, header.biWidth, header.biHeight);

    for(int i = 0; i < img->height; i++){
        if(fread(img->pixels + i * img->width, image_size_row(img), 1, in) != 1){
            fprintf(stderr, "Error from reading\n");
            return RINVALID_READ;
        }
        if (fseek(in, bmp_padding(img->width), SEEK_CUR)){
            return RNULL;
        }
    }
    return 0;
}

enum write_stat to_bmp(FILE *out, struct image *img){
    struct bmp_header header = create_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1) {
        return WERROR;
    }
    fseek(out, header.bOffBits, SEEK_SET);
    if (img->pixels != NULL) {
        for (size_t i = 0; i < img->height; i++) {
            fwrite(img->pixels + i * img->width, image_size_row(img), 1, out);
            fill_padding(img, out);
        }
    }
    return 0;
}
