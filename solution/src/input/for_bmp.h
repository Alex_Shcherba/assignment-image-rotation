#ifndef FOR_BMP_H
#define FOR_BMP_H

#include "../image/image.h"
#include <stdint.h>
#include <stdio.h>

enum write_stat {
    WERROR
};

enum read_stat {
    RINVALID_SIGNATURE,
    RINVALID_HEADER,
    RINVALID_READ,
    RINVALID_COUNT,
    RNULL
};
enum write_stat to_bmp(FILE *out, struct image *img);
enum read_stat from_bmp(FILE *in, struct image *img);


#endif 
